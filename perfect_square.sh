#!bin/bash

# a script to check whether given number is perfect square or not

clear

echo -n "Enter the number : "
read n

flag=0
i=1
square=0

while [ $i -le $n ]
do
	square=`expr $i \* $i`

	if [ $square -eq $n ]
	then
		flag=1
		echo "The number is a perfect square"
        break
	fi

    echo $i

	i=`expr $i + 1`
done

if [ $flag -eq 0 ]
then
	echo "The number is NOT a perfect square"
fi

exit