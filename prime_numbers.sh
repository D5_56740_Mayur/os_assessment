#!/bin/bash

# Write a shell script to find first n prime numbers
clear

#Initialization of counter,num
count=1
num=2

#Function for prime number
function isPrime()
{
    #Initial value for division
    i=2

    while [ $i -le $1 ]
    do
        if [ `expr $1 % $i ` -eq 0 ]
        then
            break
        fi
        i=`expr $i + 1`
    done

    if [ $i -eq $1 ]
    then
        echo "$1 "
    else
        echo "1"
    fi
}

# Function calling till number of terms
echo "Enter number of terms: "
read no

echo -n "Prime numbers :"

while [ $count -le $no ]
do
    res=$( isPrime $num )
    if [ $res -eq $num ]
    then
        echo -n "$num "
        count=`expr $count + 1`
    fi
    num=`expr $num + 1`
done

echo ""

exit