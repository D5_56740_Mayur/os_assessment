#!bin/bash

# a script to convert temperature from Celsius to Fahrenheit

clear

echo "Enter degree celsius temperature : "
read celsius

fahrenheit=`echo "scale=4; $celsius*1.8 + 32" | bc`

echo "$celsius degree celsius is equal to $fahrenheit degree fahrenheit"

exit